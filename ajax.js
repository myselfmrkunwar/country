let searchBtn = document.getElementById("search");
let countrylist = document.getElementById("countrylist");
let countryArray = [];
let filteredname = [];

function makecountryCardTemplate(country) {
  console.log(country.latlng[0], country.latlng[1]);

  return `<div class="border  shadow my-3 p-2 rounded">
            <div class="row  mx-auto align-center  justify-content-center">
                <div class="col-sm-4 px-0">
                    <img src="${country.flag}" alt="" class="img-fluid">
                </div>
                <div class="col-sm-8 px-0 px-sm-3 text-left align-self-center">
                    <div class="h2 text-secondary font-weight-bold text-capitalize mb-0">${country.name}</div>
                    <div class="h6 my-0 ">Currency: <span>${country.currencies[0].name}</span></div>
                    <div class="h6">Date and Time: <span> 26th March 2021 , 10:58</span></div>
                    <div class="row w-100  mx-auto">
                        <div class="col-sm-5  btn  btn-outline-primary my-1 px-0 mx-1 py-0  ">
                            <a class="btn w-100 " href="https://www.google.com/maps/search/?api=1&query=${country.name}"
                            target="_blank"><span>Show Map</span> </a>
                        </div>
                        <div class="col-sm-5 btn btn-outline-primary my-1 px-0 mx-1 py-0  ">
                            <a class="btn w-100 " href="details.html?cname=${country.alpha3Code}"><span>Details</span></a>
                        </div>
                    </div>
                </div>
            </div>
            </div>  `;
}

function search(coutries) {
  let input = document.getElementById("input");
  input.addEventListener("keyup", (e) => {
    let str = "";
    countrylist.innerHTML = "";
    searchValue = e.target.value.toLowerCase();
    const foundCoutries = coutries.filter((country) =>
      country.name.toLowerCase().includes(searchValue)
    );
    foundCoutries.map((country) => {
      str += makecountryCardTemplate(country);
    });
    countrylist.innerHTML = str;
  });
}

window.addEventListener("load", () => {
  const xhr = new XMLHttpRequest();
  xhr.open("GET", "https://restcountries.eu/rest/v2/all", true);
  xhr.onprogress = function () {
    console.log("on progress");
  };

  xhr.onreadystatechange = function () {
    console.log(xhr.readyState);
  };
  xhr.onload = function () {
    countryArray = JSON.parse(this.responseText);

    str = "";

    for (key in countryArray) {
      str += makecountryCardTemplate(countryArray[key]);
    }
    countrylist.innerHTML = str;
    search(countryArray);
  };
  xhr.send();
});
