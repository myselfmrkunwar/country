window.addEventListener("load", () => {
  const xhr = new XMLHttpRequest();
  let url = new URLSearchParams(window.location.search);
  urlparam = url.get("cname");

  xhr.open(
    "GET",
    `https://restcountries.eu/rest/v2/alpha/${urlparam}`,
    true
  );
  xhr.onload = function () {
    let detail = document.getElementById("detail");
    let data = JSON.parse(this.responseText);
    // let ImgUrl = data.flag.substring(0, 30);
    let neighbourCountriesArrayList = [];
    for (let i = 0; i < data.borders.length; i += 3) {
      if (i < data.borders.length) {
        neighbourCountriesArrayList.push(data.borders.slice(i, i + 3));
      }
    }

    let strNeighbourCountriesImges = "";
    // console.log(neighbourCountriesArrayList);
    if(neighbourCountriesArrayList.length > 0 ){
    neighbourCountriesArrayList.map((countriesSubArr) => {
    let colStr = "";
    
    countriesSubArr.map((countryCode) => {
        //   console.log(countryCode.toLowerCase())
        colStr += `<div class="col-4 px-1 align-self-center ">
                    <img src="https://restcountries.eu/data/${countryCode.toLowerCase()}.svg" 
                    class="img-fluid border" alt="">
                    </div>`;
        });
    strNeighbourCountriesImges += `<div class="row mx-auto py-2">
                                        ${colStr}
                                        </div>`;
    });

    }else{
        strNeighbourCountriesImges += `<div class="row mx-auto py-2">
        <div class="col-12  align-self-center ">
                    <p class="text-center h6 font-weight-light text-primary">⚠️ No Neighbours</p>
                    <img src="alone.png" 
                    class="img-fluid" alt="">
                    </div>
        </div>`;
    }

    let strLangueges = "";
    data.languages.map((language) => {
      strLangueges += language.name + " ";
    });
    let strCurrencies = "";
    data.currencies.map((currency) => {
      strCurrencies += currency.name + " ";
    });

    let str = "";
    str += `                 <div class=" row ">
    <p class="col-12 mb-0  text-secondary font-weight-bold mx-auto h1">${data.name}</p>
</div>
<div class="row  mx-auto py-2">
    <div class=" col-sm-6    align-self-center text-left px-0">
        <img src="${data.flag}" class="img-fluid  " alt="">
    </div>
    <div class=" col-sm-6  text-secondary py-2 text-capitalize px-0 px-sm-5">
        <p class="h5 font-weight-normal">native name : ${data.nativeName} </p>
        <p class="h5 font-weight-normal">capital : ${data.capital} </p>
        <p class="h5 font-weight-normal">population :${data.population} </p>
        <p class="h5 font-weight-normal">region : ${data.region}</p>
        <p class="h5 font-weight-normal">sub-region :${data.subregion} </p>
        <p class="h5 font-weight-normal">Area : ${data.area}Km <sup>2</sup> </p>
        <p class="h5 font-weight-normal">country code : +${data.callingCodes}</p>
        <p class="h5 font-weight-normal">languages : ${strLangueges}</p>
        <p class="h5 font-weight-normal">currency :${strCurrencies} </p>
        <p class="h5 font-weight-normal">timezones : ${data.timezones} </p>
    </div>
</div>
<div class="row mx-auto ">
    <div class="col-12  border">
        <p class="h4 text-capitalize text-secondary ">Neighbour country</p>
        <div class="row mx-auto ">
        ${strNeighbourCountriesImges}
        </div>

    </div>
</div>`;

    detail.innerHTML = str;
  };
  xhr.send();
});
